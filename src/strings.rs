// Primitive str = Immutable fixed-length string somewhere in memory
// String = Growable, heap-allocated data structure - use when you need to modify or own string data

pub fn run() {
    println!("Hello from the strings.rs file.");

    let h_prim = "Hello";
    let mut hello = String::from("Hello "); // Need to be mutable to add characters

    // Print primitive and object
    println!("Primitive: {}, Object: {}", h_prim, hello);

    // Get length
    println!("Length: {}", hello.len());

    // Push char
    hello.push('W');
    // Push string
    hello.push_str("orld!");

    // Capacity in bytes
    println!("Capacity: {}", hello.capacity());

    // Check if empty
    println!("Is Empty: {}", hello.is_empty());

    // Contains
    println!("Contains 'World': {}", hello.contains("World"));

    // Replace - creates and returns a new String (does not modify existing String)
    println!("Replace: {}", hello.replace("World", "There"));

    // Loop through string by whitespace
    for word in hello.split_whitespace() {
        println!("{}", word);
    }

    // Create String with capacity
    let mut s = String::with_capacity(10);
    s.push('a');
    s.push('b');
    println!("{}", s);

    // Assertion testing - this will only print out if the assertion fails
    assert_eq!(2, s.len());
    assert_eq!(10, s.capacity());
}