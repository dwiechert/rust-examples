// Arrays - Fixed list where elements are the same data types

use std::mem;

pub fn run() {
    println!("Hello from the arrays.rs file.");

    // Type is i32 and length is 5
    let mut numbers: [i32; 5] = [1, 2, 3, 4, 5]; // Mutable so we can change a value

    // Re-assign value
    numbers[2] = 20;

    println!("{:?}", numbers);

    // Get single value
    println!("{}", numbers[0]);

    // Get array length
    println!("Array length: {}", numbers.len());

    // Arrays are stack allocated
    // Full import of std::mem
    println!("Array occupies {} bytes.", std::mem::size_of_val(&numbers));
    // Using "use std::mem" from import
    println!("Array occupies {} bytes.", mem::size_of_val(&numbers));

    // Get slice
    let slice: &[i32] = &numbers[1..3];
    println!("Slice: {:?}", slice);
}