// Tuples group together values of different types
// Max 12 elements

pub fn run() {
    println!("Hello from the tuples.rs file.");

    // Need to explicity provide types
    let person: (&str, &str, i8) = ("Dan", "Chicago", 30);
    println!("{} is from {} and is {}.", person.0, person.1, person.2);
}