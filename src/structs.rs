// Structs - Used to create custom data types

// Traditional struct
struct Color {
    red: u8,
    green: u8,
    blue: u8
}

// Tuple struct
struct Color2(u8, u8, u8);

struct Person {
    first_name: String,
    last_name: String
}

impl Person {
    // Construct person
    fn new(first: &str, last: &str) -> Person {
        Person{
            first_name: first.to_string(),
            last_name: last.to_string()
        }
    }

    // Get full name (could replace &self with Person)
    fn full_name(&self) -> String {
        format!("{} {}", self.first_name, self.last_name)
    }

    // Set last name
    fn set_last_name(&mut self, last: &str) {
        self.last_name = last.to_string();
    }

    // Name to tuple
    fn to_tuple(self) -> (String, String) {
        (self.first_name, self.last_name)
    }
}

pub fn run() {
    println!("Hello from the structs.rs file.");

    let mut c = Color {
        red: 255,
        green: 0,
        blue: 0
    };

    // Can change the value since it's mutable
    c.red = 200;
    println!("Color: {} {} {}", c.red, c.green, c.blue);

    // Tuple structs don't have named fields
    let mut c2 = Color2(255, 0, 0);
    c2.0 = 200;
    println!("Color2: {} {} {}", c2.0, c2.1, c2.2);

    // Using function built on struct
    let mut p = Person::new("Mary", "Doe");
    println!("Person: {} {}", p.first_name, p.last_name);
    p.set_last_name("Smith");
    println!("Full name: {}", p.full_name());
    println!("Tuple name: {:?}", p.to_tuple());
}