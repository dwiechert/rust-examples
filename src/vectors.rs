// Vectors - Resizable arrays

use std::mem;

pub fn run() {
    println!("Hello from the vectors.rs file.");

    // Type is i32 and length is 5
    let mut numbers: Vec<i32> = vec![1, 2, 3, 4]; // Mutable so we can change a value

    // Re-assign value
    numbers[2] = 20;

    // Add on to vector
    numbers.push(5);
    numbers.push(6);

    // Pop off last value
    numbers.pop();

    println!("{:?}", numbers);

    // Get single value
    println!("{}", numbers[0]);

    // Get vector length
    println!("Vector length: {}", numbers.len());

    // Vectors are stack allocated
    // Full import of std::mem
    println!("Vector occupies {} bytes.", std::mem::size_of_val(&numbers));
    // Using "use std::mem" from import
    println!("Vector occupies {} bytes.", mem::size_of_val(&numbers));

    // Get slice
    let slice: &[i32] = &numbers[1..3];
    println!("Slice: {:?}", slice);

    // Loop through vector values
    for x in numbers.iter() {
        println!("Number: {}", x);
    }

    // Loop and mutate values
    for x in numbers.iter_mut() {
        *x *= 2;
    }

    println!("Numbers vector: {:?}", numbers);
}