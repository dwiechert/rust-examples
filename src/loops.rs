// Loops - Used to iterate until a condition is met

pub fn run() {
    println!("Hello from the loops.rs file.");

    let mut count = 0;

    // Infinite loop
    loop {
        count += 1;
        println!("Count: {}", count);

        if count == 5 {
            break;
        }
    }

    // While loop (FizzBuzz)
    while count <= 20 {
        if count % 15 == 0 {
            println!("FizzBuzz");
        } else if count % 3 == 0 {
            println!("Fizz");
        } else if count % 5 == 0 {
            println!("Buzz");
        } else {
            println!("{}", count);
        }
        count += 1;
    }

    // For range
    for x in 0..20 {
        if x % 15 == 0 {
            println!("FizzBuzz");
        } else if x % 3 == 0 {
            println!("Fizz");
        } else if x % 5 == 0 {
            println!("Buzz");
        } else {
            println!("{}", x);
        }
    }
}