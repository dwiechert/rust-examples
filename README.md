# Rust Examples

Simple examples of learning the Rust language.

Tutorial followed:

* https://youtu.be/zF34dRivLOw

To run:

```
cargo run <space separated list of words>
```

A single command **is required**.

Special commands:

* hello
* status